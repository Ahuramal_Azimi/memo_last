package com.amirmohammadazimi.memo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Amir Mohammad Azimi on 4/23/2017.
 */

public class dbConfig extends SQLiteOpenHelper {

    public final static String DB_NAME = "My_DB";
    public final static int DB_VER = 4;

    public final static String TB_NAME = "Memory";

    public final static String M_ID = "_id";
    public final static String M_TITLE = "name";
    public final static String M_DATE = "date";
    public final static String M_LOCAL = "location";
    public final static String M_DESC = "description";
    public final static String M_IMAGE = "image_data";


    public final static String CREATE_TB_MEMO = "CREATE TABLE " + TB_NAME + " ( " + M_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " + M_TITLE + " VARCHAR(32), " + M_DATE + " INTEGER, " + M_LOCAL + " VARCHAR(220) , " +
            M_DESC + " VARCHAR(220) , " + M_IMAGE + " BLOB );";

    public final static String DROP_TB_MEMO ="DROP TABLE IF EXISTS "+TB_NAME;

    public dbConfig(Context context) {
        super(context, DB_NAME, null, DB_VER);
        getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TB_MEMO);

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TB_MEMO);
            onCreate(db);

        } catch (SQLiteException e) {
            e.printStackTrace();
        }

    }
}
