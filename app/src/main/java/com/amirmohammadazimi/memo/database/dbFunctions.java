package com.amirmohammadazimi.memo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.amirmohammadazimi.memo.model.Memory;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Amir Mohammad Azimi on 4/24/2017.
 */

public class dbFunctions {

    dbConfig myConfig;

    public dbFunctions(Context context) {
        myConfig = new dbConfig(context);
        myConfig.getWritableDatabase();
    }

    public void add(Memory memory) {
        SQLiteDatabase myDB = myConfig.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(myConfig.M_TITLE, memory.getTitle());
        contentValues.put(myConfig.M_LOCAL, memory.getLocation());
        contentValues.put(myConfig.M_DATE, memory.getCalendar().getTimeInMillis());
        contentValues.put(myConfig.M_DESC, memory.getDescription());
        contentValues.put(myConfig.M_IMAGE, memory.getImage());

        myDB.insert(myConfig.TB_NAME, null, contentValues);
        myDB.close();

    }


    public ArrayList<Memory> getMemories() {
        ArrayList<Memory> DATA = new ArrayList<Memory>();
        SQLiteDatabase myDb = myConfig.getReadableDatabase();

        String[] cols = {myConfig.M_ID, myConfig.M_TITLE, myConfig.M_DATE, myConfig.M_LOCAL, myConfig.M_DESC, myConfig.M_IMAGE};

        Cursor myCursor = myDb.query(myConfig.TB_NAME, cols, null, null, null, null, null);

        Calendar calendar = Calendar.getInstance();
        while (myCursor.moveToNext()) {

            calendar.setTimeInMillis(myCursor.getInt(myCursor.getColumnIndex(myConfig.M_DATE)));
            DATA.add(new Memory(myCursor.getInt(myCursor.getColumnIndex(myConfig.M_ID)), myCursor.getString(myCursor.getColumnIndex(myConfig.M_TITLE)), myCursor.getString(myCursor.getColumnIndex(myConfig.M_LOCAL)), myCursor.getString(myCursor.getColumnIndex(myConfig.M_DESC)), myCursor.getBlob(myCursor.getColumnIndex(myConfig.M_IMAGE)), calendar));

        }

        myCursor.close();
        myDb.close();

        return DATA;
    }

    public void delete(int id) {
        SQLiteDatabase myDB = myConfig.getWritableDatabase();

        String selections = myConfig.M_ID + " =?";
        String[] whereArgs = {String.valueOf(id)};

        myDB.delete(myConfig.TB_NAME, selections, whereArgs);
        myDB.close();
    }

    public void edit(Memory memory) {
        SQLiteDatabase myDB = myConfig.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(myConfig.M_TITLE, memory.getTitle());
        contentValues.put(myConfig.M_DESC, memory.getDescription());
        contentValues.put(myConfig.M_DATE, memory.getCalendar().getTimeInMillis());
        contentValues.put(myConfig.M_IMAGE, memory.getImage());
        contentValues.put(myConfig.M_LOCAL, memory.getLocation());

        String whereClause = myConfig.M_ID + " =?";
        String[] whereArgs = {String.valueOf(memory.getId())};

        myDB.update(myConfig.TB_NAME, contentValues, whereClause, whereArgs);
        myDB.close();
    }

    public Memory getMemory(int id) {
        SQLiteDatabase myDB = myConfig.getReadableDatabase();


        String[] cols = {myConfig.M_ID, myConfig.M_TITLE, myConfig.M_DATE, myConfig.M_LOCAL, myConfig.M_DESC, myConfig.M_IMAGE};
        String selection = myConfig.M_ID + " =?";
        String[] selectionArgs = {String.valueOf(id)};
        Cursor myCursor = myDB.query(myConfig.TB_NAME, cols, selection, selectionArgs, null, null, null);

        myCursor.moveToNext();

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(myCursor.getInt(myCursor.getColumnIndex(myConfig.M_DATE)));
        Memory memory = new Memory(myCursor.getInt(myCursor.getColumnIndex(myConfig.M_ID)), myCursor.getString(myCursor.getColumnIndex(myConfig.M_TITLE)), myCursor.getString(myCursor.getColumnIndex(myConfig.M_LOCAL)), myCursor.getString(myCursor.getColumnIndex(myConfig.M_DESC)), myCursor.getBlob(myCursor.getColumnIndex(myConfig.M_IMAGE)), calendar);

        return memory;
    }
}
