package com.amirmohammadazimi.memo.model;

import java.util.Calendar;

/**
 * Created by Amir Mohammad Azimi on 4/16/2017.
 */

public class Memory {
    private int id;
    private static int count = 0;
    private String title, location, description;
    private byte[] image;
    private Calendar calendar;


    public Memory(int id, String title, String location, String description, byte[] image, Calendar calendar) {
        this.id = id;
        this.title = title;
        this.location = location;
        this.description = description;
        this.image = image;
        this.calendar = calendar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    @Override
    public String toString() {
        return title;
    }
}