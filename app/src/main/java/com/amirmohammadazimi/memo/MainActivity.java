package com.amirmohammadazimi.memo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.Toast;

import com.amirmohammadazimi.memo.database.dbConfig;
import com.amirmohammadazimi.memo.database.dbFunctions;
import com.amirmohammadazimi.memo.model.Memory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public dbFunctions memories;

    MyListAdapter myListAdapter;
    Switch adapterSwitch;
    Boolean listDisplayType;

    ListView myList;
    ArrayList<Memory> memoryArrayList;

    File memoTXTs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new dbConfig(this);

        memories = new dbFunctions(this);

        listDisplayType = false;


        myList = (ListView) findViewById(R.id.myList);
        registerForContextMenu(myList);


        memoTXTs =getTextStorageDir("Memo Text Files");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem item = menu.findItem(R.id.search_bar);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                myListAdapter.getFilter().filter(newText);

                return false;
            }
        });

        MenuItem item1 = menu.findItem(R.id.mySwitch);
        adapterSwitch = (Switch) item1.getActionView().findViewById(R.id.adapterSwitch);

        adapterSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                listDisplayType = isChecked;
                onResume();

            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("baa", "onResume: ");

        memoryArrayList = memories.getMemories();


        myListAdapter = new MyListAdapter(this, memoryArrayList, listDisplayType);
        myList.setAdapter(myListAdapter);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.myList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(memoryArrayList.get(info.position).getTitle());
            String[] menuItems = {"Edit", "Delete" , "Export txt"};
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = {"Edit", "Delete" , "Export txt"};
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = memoryArrayList.get(info.position).getTitle();

        switch (menuItemName) {
            case "Delete":
                memories.delete(memoryArrayList.get(info.position).getId());
                onResume();
                break;
            case "Edit":
                edit(memoryArrayList.get(info.position).getId());
                break;
            case "Export txt":
                writeToFile(memoryArrayList.get(info.position));
        }
        Toast.makeText(MainActivity.this, String.format("Item %s will be %sed", listItemName, menuItemName), Toast.LENGTH_SHORT).show();
        return true;
    }

    public void edit(int id) {
        Intent intent = new Intent(MainActivity.this, SavingActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("type", "edit");
        startActivity(intent);
    }

    public void openSaveActicity(View view) {
        Intent intent = new Intent(MainActivity.this, SavingActivity.class);
        intent.putExtra("type", "new");
        startActivity(intent);
    }

    public void writeToFile(Memory memory){
        File file = new File(memoTXTs, memory.getTitle()+".txt");
        file.setWritable(true);

        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(memory.getTitle());
            fileWriter.append(" - " + memory.getLocation());
            fileWriter.append(" - " + memory.getDescription());
            fileWriter.flush();
            fileWriter.close();

            Toast.makeText(this , "Saved in: "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public File getTextStorageDir(String fileName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);
        if (!file.mkdirs()) {
            Log.e("ERROR", "Directory not Created");
        }
        return file;
    }
}
